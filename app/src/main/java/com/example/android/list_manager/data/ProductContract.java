package com.example.android.list_manager.data;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public final class ProductContract {
    private ProductContract() {
    }

    public static final String CONTENT_AUTHORITY = "com.example.android.list_manager";
    public static final Uri BASE_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public static final String PATH_RECORD = "event";

    public static final class ProductEntry implements BaseColumns {
        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_URI, PATH_RECORD);
        public static final String CONTENT_LIST_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_RECORD;
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_RECORD;

        public static final String TABLE_NAME = "attendance";
        public static final String ID = BaseColumns._ID;
        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
        public static final String COMPANY = "company";
        public static final String STATUS = "status";
        public static final String IMAGE = "image";

        public static final int ARRIVED = 0;
        public static final int CANCELLED = 1;
        public static final int WAITING = 2;
        public static final int DID_NOT_PARTICIPATED = 3;

    }

}
