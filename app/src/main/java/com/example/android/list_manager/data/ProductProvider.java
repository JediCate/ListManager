package com.example.android.list_manager.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.example.android.list_manager.R;

/**
 * Created by Trompetica on 7/19/2017.
 */

public class ProductProvider extends ContentProvider {
    private static final String LOG_TAG = ProductProvider.class.getName();
    ProductDbHelper dbHelper;
    private static final int RECORDS = 100;
    private static final int RECORD_ID = 101;
    private static UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        uriMatcher.addURI(ProductContract.CONTENT_AUTHORITY, ProductContract.PATH_RECORD, RECORDS);
        uriMatcher.addURI(ProductContract.CONTENT_AUTHORITY, ProductContract.PATH_RECORD + "/#", RECORD_ID);
    }

    @Override
    public boolean onCreate() {
        dbHelper = new ProductDbHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor;
        int matcher = uriMatcher.match(uri);
        switch (matcher) {
            case RECORDS:
                cursor = db.query(ProductContract.ProductEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case RECORD_ID:
                selection = ProductContract.ProductEntry.ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                cursor = db.query(ProductContract.ProductEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            default:
                throw new IllegalArgumentException("Cannot query unknown uri: " + uri);
        }
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        final int match = uriMatcher.match(uri);
        switch (match) {
            case RECORDS:
                return ProductContract.ProductEntry.CONTENT_LIST_TYPE;
            case RECORD_ID:
                return ProductContract.ProductEntry.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri + " with match " + match);
        }
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        final int match = uriMatcher.match(uri);
        switch (match) {
            case RECORDS:
                return insertProductsTemplate(uri, values);
            default:
                throw new IllegalArgumentException("Insertion was not successful with uri: " + uri);
        }
    }

    private Uri insertProductsTemplate(Uri uri, ContentValues values) {
        //sanity checks
        String name = values.getAsString(ProductContract.ProductEntry.NAME);
        String supplier = values.getAsString(ProductContract.ProductEntry.COMPANY);

        /**
         *  if (TextUtils.isEmpty(name) || TextUtils.isEmpty(supplier) || price < 0 || quantity < 0) {
         */
        if (TextUtils.isEmpty(name) || TextUtils.isEmpty(supplier)) {
            Toast.makeText(getContext(), R.string.please_complete_all_fields, Toast.LENGTH_SHORT).show();
            return null;
        }
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        long newRowID = db.insert(ProductContract.ProductEntry.TABLE_NAME, null, values);
        if (newRowID == -1) {
            Log.e(LOG_TAG, "Something is wrong with te Uri. Failed to insert row for " + uri);
            return null;
        }
        getContext().getContentResolver().notifyChange(uri, null);

        return ContentUris.withAppendedId(uri, newRowID);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        int rowsDeleted;
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        final int match = uriMatcher.match(uri);
        switch (match) {
            case RECORDS:
                rowsDeleted = db.delete(ProductContract.ProductEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case RECORD_ID:
                selection = ProductContract.ProductEntry.ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                rowsDeleted = db.delete(ProductContract.ProductEntry.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Deletion is not supported for uri: " + uri);
        }
        if (rowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsDeleted;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {

        final int match = uriMatcher.match(uri);
        switch (match) {
            case RECORDS:
                return updateProduct(uri, values, selection, selectionArgs);

            case RECORD_ID:
                selection = ProductContract.ProductEntry.ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                return updateProduct(uri, values, selection, selectionArgs);

            default:
                throw new IllegalArgumentException("Update attempt failed with uri: " + uri);
        }


    }

    private int updateProduct(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        //sanity checks
        if (values.size() == 0) {
            return 0;
        }
        if (values.containsKey(ProductContract.ProductEntry.NAME) && TextUtils.isEmpty(values.getAsString(ProductContract.ProductEntry.NAME))) {
            Toast.makeText(getContext(), R.string.enter_name_to_update, Toast.LENGTH_LONG).show();
            return 0;
        }
        if (values.containsKey(ProductContract.ProductEntry.COMPANY) && TextUtils.isEmpty(values.getAsString(ProductContract.ProductEntry.COMPANY))) {
            Toast.makeText(getContext(), R.string.enter_supplier, Toast.LENGTH_LONG).show();
            return 0;
        }

        //perform update
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int newRows = db.update(ProductContract.ProductEntry.TABLE_NAME, values, selection, selectionArgs);

        if (newRows != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return newRows;
    }

}
