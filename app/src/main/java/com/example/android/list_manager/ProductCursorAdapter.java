package com.example.android.list_manager;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.list_manager.data.ProductContract;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductCursorAdapter extends CursorAdapter {
    private static final String LOG_TAG = ProductCursorAdapter.class.getName();

    public ProductCursorAdapter(Context context, Cursor cursor) {

        super(context, cursor, 0);

    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_record, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        view.setTag(viewHolder);
        return view;
    }

    @Override
    public void bindView(View view, final Context context, final Cursor cursor) {
        final ViewHolder viewHolder = (ViewHolder) view.getTag();

        final long currentId = cursor.getInt(cursor.getColumnIndex(ProductContract.ProductEntry.ID));
        String company = cursor.getString(cursor.getColumnIndex(ProductContract.ProductEntry.COMPANY));
        String getName = cursor.getString(cursor.getColumnIndex(ProductContract.ProductEntry.NAME));
        final int getStatus= cursor.getInt(cursor.getColumnIndex(ProductContract.ProductEntry.STATUS));
        byte[] getImageAsByte = cursor.getBlob(cursor.getColumnIndex(ProductContract.ProductEntry.IMAGE));

        String statusString = context.getString(R.string.type);
        switch (getStatus) {
            case 0:
                statusString += context.getString(R.string.arrived);
                viewHolder.status_icon.setImageResource(R.drawable.arrived);
                viewHolder.statusTv.setTextColor(Color.rgb(37,165,8));
                break;
            case 1:
                statusString += context.getString(R.string.cancelled);
                viewHolder.status_icon.setImageResource(R.drawable.cancelled);
                viewHolder.statusTv.setTextColor(Color.rgb(178,8,8));
                break;
            case 2:
                statusString += context.getString(R.string.waiting);
                viewHolder.status_icon.setImageResource(R.drawable.waiting);
                viewHolder.statusTv.setTextColor(Color.rgb(249,104,7));
                break;
            case 3:
                statusString += context.getString(R.string.missing);
                viewHolder.status_icon.setImageResource(R.drawable.cancelled);
                viewHolder.statusTv.setTextColor(Color.rgb(178,8,8));
                break;
        }
        viewHolder.statusTv.setText(statusString);
        viewHolder.name.setText(getName);

        String companyString = "Company: " + company;
        viewHolder.company.setText(companyString);

        Bitmap productImage = BitmapFactory.decodeByteArray(getImageAsByte, 0, getImageAsByte.length);
        viewHolder.image.setImageBitmap(productImage);

        viewHolder.arrived_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context, "You made a SELL", Toast.LENGTH_SHORT).show();
                Uri updateUri = ContentUris.withAppendedId(ProductContract.ProductEntry.CONTENT_URI, currentId);
                // Toast.makeText(context, "update URi from SELL: " + updateUri, Toast.LENGTH_LONG).show();
                updateStatus(context, updateUri, viewHolder);


            }
        });
    }

    public static class ViewHolder {
        @BindView(R.id.item_name)
        TextView name;
        @BindView(R.id.item_status)
        TextView statusTv;
        @BindView(R.id.item_company)
        TextView company;
        @BindView(R.id.item_image)
        ImageView image;
        @BindView(R.id.item_status_icon)
        ImageView status_icon;
        @BindView(R.id.arrived_btn)
        Button arrived_btn;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    private void updateStatus(Context context, Uri currentUri, ViewHolder viewHolder) {
       String statusString = null;
        ContentValues values = new ContentValues();
        values.put(ProductContract.ProductEntry.STATUS, 0);

        int rowsUpdated = context.getContentResolver().update(currentUri, values, null, null);
        if (rowsUpdated == 0) {
            Toast.makeText(context, R.string.sell_update_fail, Toast.LENGTH_SHORT).show();
        } else {
            statusString = context.getString(R.string.arrived);
            viewHolder.status_icon.setImageResource(R.drawable.arrived);
            viewHolder.statusTv.setText(statusString);
            viewHolder.statusTv.setTextColor(Color.rgb(37,165,8));
            Toast.makeText(context, R.string.sell_update_success + statusString, Toast.LENGTH_SHORT).show();
        }
    }
}
