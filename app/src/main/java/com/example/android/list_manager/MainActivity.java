package com.example.android.list_manager;

import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.android.list_manager.data.ProductContract;

import java.io.ByteArrayOutputStream;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final String LOG_TAG = MainActivity.class.getName();
    public static final int LOADER_ID = 0;
    //ListView productList;
    ProductCursorAdapter adapter;
    String[] PROJECTION = new String[]{
            ProductContract.ProductEntry._ID,
            ProductContract.ProductEntry.NAME,
            ProductContract.ProductEntry.DESCRIPTION,
            ProductContract.ProductEntry.COMPANY,
            ProductContract.ProductEntry.STATUS,
            ProductContract.ProductEntry.IMAGE
    };
    static final String SELECTION = "((" + ProductContract.ProductEntry.NAME + " NOTNULL) AND ( "
            + ProductContract.ProductEntry.NAME + "!= ''))";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //setup fab button
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, EditorActivity.class);
                startActivity(intent);
            }
        });

        //setup empty view
        View empty_tv = findViewById(R.id.empty_view);
        ListView productList = (ListView) findViewById(R.id.cream_list);
        productList.setEmptyView(empty_tv);

        adapter = new ProductCursorAdapter(this, null);
        productList.setAdapter(adapter);
        productList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.v(LOG_TAG, "clicked");
                Intent intent = new Intent(MainActivity.this, EditorActivity.class);
                //send the URI t the editor activity to get the company of the producta that was clicked on
                Uri contentProductUri = ContentUris.withAppendedId(ProductContract.ProductEntry.CONTENT_URI, id);
                intent.setData(contentProductUri);
                startActivity(intent);
            }
        });

        getLoaderManager().initLoader(LOADER_ID, null, this);
    }

    private void insertBasicData() {
        Drawable image1 = ContextCompat.getDrawable(this, R.drawable.joe1);
        addDataTemplate(convertToByte(image1), getString(R.string.joe1), null, getString(R.string.vinart), 0);

        Drawable image3 = ContextCompat.getDrawable(this, R.drawable.joe2);
        addDataTemplate(convertToByte(image3), getString(R.string.joe2), null, getString(R.string.vinart), 1);

        Drawable image7 = ContextCompat.getDrawable(this, R.drawable.joe3);
        addDataTemplate(convertToByte(image7), getString(R.string.joe3), null, getString(R.string.vinart),  2);

        Drawable image8 = ContextCompat.getDrawable(this, R.drawable.joe4);
        addDataTemplate(convertToByte(image8), getString(R.string.joe4), null, getString(R.string.vinart), 3);
    }

    private void addDataTemplate(byte[] image, String name, String description, String supplier, int status) {
        ContentValues values = new ContentValues();
        values.put(ProductContract.ProductEntry.NAME, name);
        values.put(ProductContract.ProductEntry.DESCRIPTION, description);
        values.put(ProductContract.ProductEntry.COMPANY, supplier);
        values.put(ProductContract.ProductEntry.STATUS, status);
        values.put(ProductContract.ProductEntry.IMAGE, image);

        Uri insertUri = getContentResolver().insert(ProductContract.ProductEntry.CONTENT_URI, values);
        //catch potential errors
        Log.v(LOG_TAG, "New URI in insertData " + insertUri);

    }

    private byte[] convertToByte(Drawable imageFromFile) {
        //Convert to bitmap
        BitmapDrawable bitmapDrawable = ((BitmapDrawable) imageFromFile);
        Bitmap bitmap = bitmapDrawable.getBitmap();
        //Convert to byte to store
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
        byte[] imageByte = bos.toByteArray();
        return imageByte;
    }

    private void deleteAllRecords() {
        int deletedRows = getContentResolver().delete(ProductContract.ProductEntry.CONTENT_URI, null, null);
        Log.v(LOG_TAG, "Deleted number of rows  " + deletedRows);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_insert_dummy_data:
                insertBasicData();
                return true;
            case R.id.action_delete_all_entries:
                deleteAllRecords();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this, ProductContract.ProductEntry.CONTENT_URI, PROJECTION, SELECTION, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }
}
