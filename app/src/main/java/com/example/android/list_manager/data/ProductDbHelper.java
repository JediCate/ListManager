package com.example.android.list_manager.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class ProductDbHelper extends SQLiteOpenHelper {
    private static final String LOG_TAG = ProductDbHelper.class.getName();
    private static final String DATABASE_NAME = "list_manager.db";
    private static final int DATABASE_VERSION = 1;

    public ProductDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String SQL_CREATE_ENTRIES = "CREATE TABLE " + ProductContract.ProductEntry.TABLE_NAME + "("
                + ProductContract.ProductEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ProductContract.ProductEntry.NAME + " TEXT NOT NULL, "
                + ProductContract.ProductEntry.DESCRIPTION + " TEXT, "
                + ProductContract.ProductEntry.COMPANY + " TEXT NOT NULL, "
                + ProductContract.ProductEntry.STATUS + " INTEGER NOT NULL DEFAULT 2, "
                + ProductContract.ProductEntry.IMAGE + " BLOB);";

        db.execSQL(SQL_CREATE_ENTRIES);
        Log.v(LOG_TAG, "SCHEMA: " + SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String DELETE_ENTRIES = "DROP TABLE " + ProductContract.ProductEntry.TABLE_NAME;
        db.execSQL(DELETE_ENTRIES);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
