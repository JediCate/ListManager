package com.example.android.list_manager;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.android.list_manager.data.ProductContract;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditorActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final String LOG_TAG = EditorActivity.class.getName();
    private static final int EXISTING_LOADER = 1;
    private int REQUEST_CAMERA = 0;
    private int SELECT_FILE = 1;

    @BindView(R.id.image_edit)
    ImageView imageInsert;
    @BindView(R.id.name_edit)
    EditText getNameEdit;
    @BindView(R.id.description_edit)
    EditText getDescriptionEdit;
    @BindView(R.id.supplier_edit)
    EditText getSupplierEdit;

    @BindView(R.id.status_type)
    Spinner getStatusSpinner;
    @BindView(R.id.notify_btn)
    Button getOrder;

    private boolean productHasChanged = false;
    public String userChoice = "";
    private int status = ProductContract.ProductEntry.WAITING;
    private Uri currentProductUri;
    private View.OnTouchListener touchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            productHasChanged = true;
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);
        ButterKnife.bind(this);

        getNameEdit.setOnTouchListener(touchListener);
        getDescriptionEdit.setOnTouchListener(touchListener);
        getSupplierEdit.setOnTouchListener(touchListener);
        getStatusSpinner.setOnTouchListener(touchListener);
        imageInsert.setOnTouchListener(touchListener);

        currentProductUri = getIntent().getData();

        //here starts the image odyssey
        imageInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        getOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                /**
                 * if statusn arrived
                 */
                contactSupplier();
            }
        });

        if (currentProductUri == null) {
            setTitle(getString(R.string.editor_setTitle_new));
            invalidateOptionsMenu();
            getOrder.setVisibility(View.GONE);
            /*getQuantityEdit.setText("0");*/
        } else {
            setTitle(getString(R.string.editor_setTitle_existing));
            getOrder.setVisibility(View.VISIBLE);
            getLoaderManager().initLoader(EXISTING_LOADER, null, this);
        }
        setupSpinner();
    }

    /**
     * Image capture and processing section
     */
    private void selectImage() {
        final String[] options = {getString(R.string.take_photo), getString(R.string.chose_from_gallery), getString(R.string.cancel)};
        final AlertDialog.Builder builder = new AlertDialog.Builder(EditorActivity.this);
        builder.setTitle(R.string.add_photo);
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = PermissionsUtility.checkPermissions(EditorActivity.this);
                if (options[item].equals(getString(R.string.take_photo))) {
                    userChoice = getString(R.string.take_photo);
                    if (result) {
                        cameraIntent();
                    }
                } else if (options[item].equals(getString(R.string.chose_from_gallery))) {
                    userChoice = getString(R.string.chose_from_gallery);
                    if (result) {
                        galleryIntent();
                    }
                } else {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PermissionsUtility.PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoice == getString(R.string.take_photo)) {
                        cameraIntent();
                    } else if (userChoice == getString(R.string.chose_from_gallery)) {
                        galleryIntent();
                    }
                } else {
                    Toast.makeText(this, R.string.editor_camera_permissions, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent() {
        //Ation_get_Content --> error when trying to import png (other format then jpg)
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent.createChooser(intent, getString(R.string.select_file)), SELECT_FILE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                onSelectFromGalleryResult(data);
            } else if (requestCode == REQUEST_CAMERA) {
                onCaptureImageResult(data);
            }
        }
    }

    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bitmap = null;
        if (data != null) {
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        imageInsert.setImageBitmap(bitmap);
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
        FileOutputStream fileOutputStream;
        try {
            destination.createNewFile();
            fileOutputStream = new FileOutputStream(destination);
            fileOutputStream.write(bytes.toByteArray());
            fileOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        imageInsert.setImageBitmap(thumbnail);
    }

    private void setupSpinner() {
        ArrayAdapter skinTypeSpinnerAdapter = ArrayAdapter.createFromResource(this, R.array.skin_type_options, android.R.layout.simple_spinner_item);
        skinTypeSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        getStatusSpinner.setAdapter(skinTypeSpinnerAdapter);

        getStatusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                if (!TextUtils.isEmpty(selection)) {
                    if (selection.equals(getString(R.string.arrived))) {
                        status = ProductContract.ProductEntry.ARRIVED;
                    } else if (selection.equals(getString(R.string.cancelled))) {
                        status = ProductContract.ProductEntry.CANCELLED;
                    } else if (selection.equals(getString(R.string.missing))) {
                        status = ProductContract.ProductEntry.DID_NOT_PARTICIPATED;
                    }else{
                        status = ProductContract.ProductEntry.WAITING;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                status = ProductContract.ProductEntry.WAITING;
            }
        });
    }

    private byte[] prepareImageForDatabase() {
        Drawable productImage;
        if (imageInsert.getDrawable() == null) {
            productImage = ContextCompat.getDrawable(this, R.drawable.image_template);
        } else {
            //Read input fields
            productImage = imageInsert.getDrawable();
        }
        //Convert to bitmap
        BitmapDrawable bitmapDrawable = ((BitmapDrawable) productImage);
        Bitmap bitmap = bitmapDrawable.getBitmap();
        //Convert to byte to store
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
        byte[] imageByte = bos.toByteArray();

        return imageByte;
    }

    private boolean saveData() {
        byte[] imageValue = prepareImageForDatabase();
        String name = getNameEdit.getText().toString().trim();
        String description = getDescriptionEdit.getText().toString().trim();
        String supplier = getSupplierEdit.getText().toString().trim();

        //if you are trying to save a new product, check that all fields are completed --> && TextUtils.isEmpty(price) && TextUtils.isEmpty(quantity)
        if (currentProductUri == null && TextUtils.isEmpty(name) && TextUtils.isEmpty(supplier)) {
            //Toast.makeText(this, "This is a new product: name, supplier, price and qty cannot be empty. SAVE DATA returned FALSE ", Toast.LENGTH_SHORT).show();
            return false;
        }

        ContentValues values = new ContentValues();
        values.put(ProductContract.ProductEntry.NAME, name);
        values.put(ProductContract.ProductEntry.DESCRIPTION, description);
        values.put(ProductContract.ProductEntry.COMPANY, supplier);
        values.put(ProductContract.ProductEntry.STATUS, status);
        values.put(ProductContract.ProductEntry.IMAGE, imageValue);

        //if this is a new product
        if (currentProductUri == null) {
            Uri newUri = getContentResolver().insert(ProductContract.ProductEntry.CONTENT_URI, values);
            if (newUri == null) {
                Toast.makeText(this, getString(R.string.error_save_data), Toast.LENGTH_SHORT).show();
                return false;
            } else {
                Toast.makeText(this, getString(R.string.new_product_saved), Toast.LENGTH_SHORT).show();
            }
        } else { //we are updating an existing product
            int rowsUpdated = getContentResolver().update(currentProductUri, values, null, null);
            if (rowsUpdated == 0) {
                Toast.makeText(this, R.string.error_updating, Toast.LENGTH_SHORT).show();
                return false;
            } else {
                Toast.makeText(this, getString(R.string.update_successful), Toast.LENGTH_SHORT).show();
            }
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (!productHasChanged) {
            super.onBackPressed();
            return;
        }
        DialogInterface.OnClickListener discarButtonOnClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // User clicked "Discard" button, close the current activity.
                finish();
            }
        };
        showUnsavedChangesDialog(discarButtonOnClickListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_editor, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (currentProductUri == null) {
            MenuItem item = menu.findItem(R.id.action_delete);
            item.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                if (saveData()) {
                    finish();
                } else {
                    Toast.makeText(this, R.string.complete_all_fields, Toast.LENGTH_SHORT).show();
                }
                return true;
            case R.id.action_delete:
                showDeleteConfirmationDialog();
                return true;
            case R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String[] projection = new String[]{
                ProductContract.ProductEntry.ID,
                ProductContract.ProductEntry.NAME,
                ProductContract.ProductEntry.DESCRIPTION,
                ProductContract.ProductEntry.COMPANY,
                ProductContract.ProductEntry.STATUS,
                ProductContract.ProductEntry.IMAGE
        };
        return new CursorLoader(this, currentProductUri, projection, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        // Bail early if the cursor is null or there is less than 1 row in the cursor
        if (cursor == null || cursor.getCount() < 1) {
            return;
        }
        if (cursor.moveToFirst()) {
            byte[] image = cursor.getBlob(cursor.getColumnIndex(ProductContract.ProductEntry.IMAGE));
            Bitmap imageBitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
            imageInsert.setImageBitmap(imageBitmap);

            String name = cursor.getString(cursor.getColumnIndex(ProductContract.ProductEntry.NAME));
            getNameEdit.setText(name);

            String description = cursor.getString(cursor.getColumnIndex(ProductContract.ProductEntry.DESCRIPTION));
            getDescriptionEdit.setText(description);

            String supplier = cursor.getString(cursor.getColumnIndex(ProductContract.ProductEntry.COMPANY));
            getSupplierEdit.setText(supplier);

            int statusType = cursor.getInt(cursor.getColumnIndex(ProductContract.ProductEntry.STATUS));
            switch (statusType) {
                case ProductContract.ProductEntry.ARRIVED:
                    getStatusSpinner.setSelection(0);
                    break;
                case ProductContract.ProductEntry.CANCELLED:
                    getStatusSpinner.setSelection(1);
                    break;
                case ProductContract.ProductEntry.WAITING:
                    getStatusSpinner.setSelection(2);
                    break;
                case ProductContract.ProductEntry.DID_NOT_PARTICIPATED:
                    getStatusSpinner.setSelection(3);
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        getNameEdit.setText("");
        getDescriptionEdit.setText("");
        getSupplierEdit.setText("");
        getStatusSpinner.setSelection(2);
        imageInsert.setImageResource(R.drawable.image_template);
    }

    private void showUnsavedChangesDialog(DialogInterface.OnClickListener discardButtonClickListener) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.discard_message);
        builder.setNegativeButton(R.string.discard, discardButtonClickListener);
        builder.setPositiveButton(R.string.keep_editing, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // User clicked the "Keep editing" button, so dismiss the dialog
                // and continue editing the pet.
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showDeleteConfirmationDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.delete_dialog_title);
        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton(R.string.delete_product, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteProduct();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void deleteProduct() {
        if (currentProductUri != null) {
            int deletedRows = getContentResolver().delete(currentProductUri, null, null);
            if (deletedRows == 0) {
                Toast.makeText(this, getString(R.string.editor_delete_fail), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, getString(R.string.editor_delete_success), Toast.LENGTH_SHORT).show();
            }
        }
        finish();
    }

    private void contactSupplier() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage(R.string.contact_supplier_title);
        builder.setPositiveButton(R.string.phone, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                } else {
                    Toast.makeText(EditorActivity.this, R.string.phone_broken, Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.setNegativeButton(R.string.email, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String name = getNameEdit.getText().toString().trim();
                String statusString = null;
                switch(status){
                    case 0:
                        statusString = getString(R.string.arrived);
                        break;
                    case 1:
                        statusString = getString(R.string.cancelled);
                        break;
                    case 2:
                        statusString = getString(R.string.waiting);
                        break;
                    case 3:
                        statusString = getString(R.string.missing);
                        break;
                }

                if (TextUtils.isEmpty(name)) {
                    Toast.makeText(EditorActivity.this, R.string.email_data_check, Toast.LENGTH_SHORT).show();
                } else {
                    String title = getString(R.string.new_order) + name;
                    String emailMessage = "Statusul Dl/Dnei " + name + " la eveniment s-a schimbat in: " + statusString +  getString(R.string.thank_you);

                    Intent intent = new Intent(Intent.ACTION_SENDTO);
                    intent.setData(Uri.parse("mailto:"));
                    intent.putExtra(Intent.EXTRA_SUBJECT, title);
                    intent.putExtra(Intent.EXTRA_TEXT, emailMessage);
                    if (intent.resolveActivity(getPackageManager()) != null) {
                        startActivity(intent);
                    }
                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }


}
